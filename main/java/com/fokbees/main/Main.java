package main.java.com.fokbees.main;

import main.java.com.fokbees.People;
import main.java.com.fokbees.main.events.Commands;
import main.java.com.fokbees.main.events.Events;
import main.java.com.fokbees.main.events.KimEvents;
import sx.blah.discord.api.ClientBuilder;
import sx.blah.discord.api.IDiscordClient;
import sx.blah.discord.api.events.EventDispatcher;
import sx.blah.discord.util.DiscordException;

public class Main {
	  
	    public static void main(String[] args) {
	    	People.Init();
	    	Config.load();
	        IDiscordClient client = createClient(Config.load(), true); 
	        EventDispatcher dispatcher = client.getDispatcher(); 
	        dispatcher.registerListener(new Events()); 
	        dispatcher.registerListener(new Commands()); 
	        dispatcher.registerListener(new KimEvents()); 
		}
	
    public static IDiscordClient createClient(String token, boolean login) { 
        ClientBuilder clientBuilder = new ClientBuilder();
        clientBuilder.withToken(token);
        try {
            if (login) {
                return clientBuilder.login(); 
            } else {
                return clientBuilder.build();
            }
        } catch (DiscordException e) { 
            e.printStackTrace();
            return null;
        }
    }
}