package main.java.com.fokbees.main;

import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.Properties;

public class Config {
	
  public static void save() {

	Properties prop = new Properties();
	OutputStream output = null;

	try {

		output = new FileOutputStream("config.txt");

		prop.setProperty("bot_id", "ha");
		prop.store(output, null);

	} catch (IOException io) {
		io.printStackTrace();
	} finally {
		if (output != null) {
			try {
				output.close();
			} catch (IOException e) {
				e.printStackTrace();
			}
		}

	}
  }
  
  
  
  public static String load() {

		Properties prop = new Properties();
		InputStream input = null;

		try {

			input = new FileInputStream("config.txt");

			prop.load(input);

			System.out.println(prop.getProperty("bot_id"));

		} catch (IOException ex) {
			ex.printStackTrace();
		} finally {
			if (input != null) {
				try {
					input.close();
				} catch (IOException e) {
					e.printStackTrace();
				}
			}
		}
		return prop.getProperty("bot_id");

	  }
  
  
  
  
  
  
  
  
  
  
  
}